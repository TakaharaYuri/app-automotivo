import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertController, LoadingController } from 'ionic-angular';

@Injectable()
export class GlobalProvider {

    public apiURL = "https://dearlerforce.com.br/";
    // public apiURL = "http://localhost/working/crm-key-beta/";
    public clientID = "NldGMktOdEVNQlZ0SlJ1VmQ0V0FJdz09";

    public static load = null;

    constructor(
        public http: HttpClient,
        public alertCtrl: AlertController,
        public loadCtrl: LoadingController
      ) {
  
    }

    public seminovos() {
        return this.http.get(this.apiURL + "seminovos/api/get_modelos/" + this.clientID + "/");
    }
    

    public seminovos_detalhe(url){
        let data = {
            "url": url
          }
    
          return this.http.post(this.apiURL + "seminovos/api/get_detalhes_modelo/"+this.clientID, data, {headers: {'Content-Type': 'multipart/form-data'}});
    }

     public novos() {
      let data = {
        "key": this.clientID
      }

      return this.http.post(this.apiURL + "produtos/api/get_produtos", data, {headers: {'Content-Type': 'application/json'}});
    }

    public showAlert(message,title=null) {
        if (!title) title = "Alerta!";
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: message,
          buttons: ['OK']
        });
        return alert.present();
    }

    public startLoading(message) {
        if (GlobalProvider.load) {
            GlobalProvider.load.setContent(message);
        }
        else {
            GlobalProvider.load = this.loadCtrl.create({
                content: message
            });
            GlobalProvider.load.present();
        }
    }

    public stopLoading() {
        if (GlobalProvider.load) {
            GlobalProvider.load.dismiss();
        }
        GlobalProvider.load = null;
    }

}
