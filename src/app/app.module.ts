import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { MainPage } from '../pages/main/main';
import { GlobalProvider } from '../providers/global/global';
import { SeminovoPage } from '../pages/seminovo/seminovo';
import { ServicosPage } from '../pages/servicos/servicos';
import { NovosPage } from '../pages/novos/novos';
import { NovoDetalhePage } from '../pages/novo-detalhe/novo-detalhe';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { SeminovoDetalhePage } from '../pages/seminovo-detalhe/seminovo-detalhe';
import { SocialLoginPage } from '../pages/social-login/social-login';
import { InformacoesPage } from '../pages/informacoes/informacoes';
import { AgendamentosPage } from '../pages/agendamentos/agendamentos';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    MainPage,
    SeminovoPage,
    SeminovoDetalhePage,
    SocialLoginPage,
    ServicosPage,
    NovosPage,
    NovoDetalhePage,
    InformacoesPage,
    AgendamentosPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    MainPage,
    SeminovoPage,
    SeminovoDetalhePage,
    SocialLoginPage,
    ServicosPage,
    NovosPage,
    NovoDetalhePage,
    InformacoesPage,
    AgendamentosPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HttpClient,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GlobalProvider
  ]
})
export class AppModule {}
