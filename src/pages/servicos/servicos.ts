import { Component } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { NavController, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';

@Component({
    selector: 'page-servicos',
    templateUrl: 'servicos.html'
})
export class ServicosPage {

    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public global: GlobalProvider
    ) {
        
    }

}
