import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MainPage } from '../main/main';
import { SeminovoPage } from '../seminovo/seminovo';
import { ServicosPage } from '../servicos/servicos';
import { NovosPage } from '../novos/novos';
import { InformacoesPage } from '../informacoes/informacoes';
import { AgendamentosPage } from '../agendamentos/agendamentos';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  mainPage = MainPage;
  seminovoPage = SeminovoPage;
  servicosPage = ServicosPage;
  novosPage = NovosPage;
  informacoesPage = InformacoesPage;
  agendamentosPage = AgendamentosPage;

  constructor(public navCtrl: NavController) {

  }

}
