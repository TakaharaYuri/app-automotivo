import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { SocialLoginPage } from '../social-login/social-login';

@Component({
    selector: 'page-seminovo-detalhe',
    templateUrl: 'seminovo-detalhe.html',
})
export class SeminovoDetalhePage {

    public url_seminovo;  
    public seminovo;

    constructor(
        public navCtrl: NavController, 
        public modal: ModalController,
        public navParams: NavParams,
        public global: GlobalProvider
    ) {
        
    }

    ionViewDidLoad() {
        this.url_seminovo = this.navParams.get('url');
        this.global.startLoading("Carregando dados...");
        this.global.seminovos_detalhe(this.url_seminovo).subscribe((response:any) => {
            this.seminovo = response.data;
            this.global.stopLoading();
            console.log(this.seminovo);
        });
        this.global.stopLoading(); 

    }

    public interessado() {
      let message = "Para registramos seu interesse, faça seu cadastro!";
      let callback = () => {
          this.global.showAlert("Se interesse foi registrado!");
      }
      
      this.navCtrl.push(SocialLoginPage,{ message:message, callback:callback });
    }

}
