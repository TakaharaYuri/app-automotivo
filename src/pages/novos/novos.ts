import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { NovoDetalhePage } from '../novo-detalhe/novo-detalhe';

@Component({
    selector: 'page-novos',
    templateUrl: 'novos.html'
})
export class NovosPage {
        public novos = null;
    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public global: GlobalProvider
    ) {
        
    }

     ionViewDidLoad() {
        this.global.startLoading("Carregando Modelos...");
        this.global.novos().subscribe((response:any) => {
            this.novos = response.data;
            this.global.stopLoading();
            console.log(this.novos);
        });
    }

    public visualizarProduto(veiculo) {
        this.navCtrl.push(NovoDetalhePage,{veiculo:veiculo });
    }

}
