import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { SeminovoDetalhePage } from '../seminovo-detalhe/seminovo-detalhe';

@Component({
    selector: 'page-seminovo',
    templateUrl: 'seminovo.html'
})
export class SeminovoPage {

    public seminovos = null;

    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public global: GlobalProvider
    ) {
        
    }

    ionViewDidLoad() {
        this.global.startLoading("Carregando dados...");
        this.global.seminovos().subscribe((response:any) => {
            this.seminovos = response.data;
            this.global.stopLoading();
            console.log(this.seminovos);
        });
    }

    public visualizarSeminovo(url) {
        this.global.startLoading("Carregando Detalhes...");
        this.navCtrl.push(SeminovoDetalhePage,{ url:url });
    }

}
