import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { SocialLoginPage } from '../social-login/social-login';

@Component({
    selector: 'page-novo-detalhe',
    templateUrl: 'novo-detalhe.html',
})
export class NovoDetalhePage {

    public veiculo; 

    constructor(
        public navCtrl: NavController, 
        public modal: ModalController,
        public navParams: NavParams,
        public global: GlobalProvider
    ) {
        
    }

    ionViewDidLoad() {
        this.veiculo = this.navParams.get('veiculo');
        this.global.stopLoading();
        console.log(this.veiculo); 

    }

    public interessado() {
      let message = "Para que você possa ter acesso a ofertas exclusivas, primeiro você precisa criar uma conta.";
      let callback = () => {
          this.global.showAlert("Se interesse foi registrado!");
      }
      
      this.navCtrl.push(SocialLoginPage,{ message:message, callback:callback });
    }

}
