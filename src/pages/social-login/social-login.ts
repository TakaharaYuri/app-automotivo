import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';

@Component({
  selector: 'page-social-login',
  templateUrl: 'social-login.html',
})
export class SocialLoginPage {

    public message = "Selecione a forma de Logon:";
    public callback = null;

    public entity:any = {
        PCL_Nome:null,
        PCL_Email:null,
        PCL_DataNascimento:null
    };

    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public global: GlobalProvider
    ) {

    }

    ionViewDidLoad() {
        let message = this.navParams.get("message");
        this.callback = this.navParams.get("callback");
        if (message) {
            this.message = message;
        }
    }
    
    /****
    public facebookLogin() {
        let logged = this.facebook.login(['public_profile', 'email']).then((response) => {
            this.global.startLoading("Logando usando Facebook....")
            this.global.socialLogin(response,"FACEBOOK").subscribe((data) => {
                console.log(data);
                localStorage.setItem("usuario",JSON.stringify(data));
                GlobalProvider.usuario = data;
                if (GlobalProvider.token != null) {
                    this.global.registrarToken(GlobalProvider.token).subscribe(() => {

                    });
                }
                this.navCtrl.pop();
                this.global.stopLoading();
                if (this.callback) this.callback();
            });
            return true;      
        }).catch((error) => { 
            console.log(error);
            this.global.showAlert(JSON.stringify(error));
            return false; 
        }); 
        return logged; 
    }

    public googleLogin() {
        this.googlePlus.login({fields:['birthday']}).then((response) => {
            this.global.startLoading("Logando usando Google+...");
            this.global.socialLogin(response,"GOOGLE").subscribe((data) => {
                console.log(data);
                localStorage.setItem("usuario",JSON.stringify(data));
                GlobalProvider.usuario = data;
                if (GlobalProvider.token != null) {
                    this.global.registrarToken(GlobalProvider.token).subscribe(() => {

                    });
                }
                this.navCtrl.pop();
                this.global.stopLoading();
                if (this.callback) this.callback();
            });
        }).catch((error) => {
            console.log(error);
            this.global.showAlert(JSON.stringify(error));
            return false; 
        });
    }
    /****/

    public manualLogin() {
        let ok = true;
        for (let key in this.entity) {
            if (key != "PCL_DataNascimento") {
                if (!this.entity[key]) {
                    ok = false;
                }
            }
        }

        if (!ok) {
            this.global.showAlert("Dados incorretos, tente novamente.");
        }
        else {
            this.navCtrl.pop();
            if (this.callback) this.callback();
            /****
            this.global.startLoading("Registrando usuário...");
            this.global.socialLogin(this.entity,"JPE").subscribe((data) => {
                console.log(data);
                localStorage.setItem("usuario",JSON.stringify(data));
                GlobalProvider.usuario = data;
                if (GlobalProvider.token != null) {
                    this.global.registrarToken(GlobalProvider.token).subscribe(() => {

                    });
                }
                this.navCtrl.pop();
                this.global.stopLoading();
                if (this.callback) this.callback();
            });
            /****/
        }
    }



}
