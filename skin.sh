#!/bin/bash
client=$1
echo "##### GERANDO APP COM SKIN DE $client  #####"
echo "##### CRIANDO BACKUP DO META DADOS  #####"

cp config.xml skin/main

echo "##### PREPARANDO OS RECURSOS  #####"
cp skin/$client/icon.png resources
cp skin/$client/splash.png resources
cp skin/$client/img/* src/assets/imgs
rm -R resources/android/*
rm -R resources/ios/*
ionic cordova resources

config=$(<config.xml)

input="skin/$client/info.txt"
echo "##### MONTANDO METADADOS DO CLIENTE  #####"
while IFS= read -r var
do
  find="$(cut -d'=' -f1 <<<"$var")"
  replace="$(cut -d'=' -f2 <<<"$var")"
  config=${config//$find/$replace}
done < "$input"

echo "$config" > config.xml
echo "$config" > config-$client.xml

ionic cordova build android --prod

cp skin/main/config.xml ./config.xml

mv C:\\Mobile\\Builds\\app-debug.apk C:\\Mobile\\Builds\\$client-debug.apk