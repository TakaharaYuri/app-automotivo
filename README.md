#App Automotivo
###Diagrama de Navegação


#### Layout
	O layout será no formato padrão do G3 Automotive com algumas alterações.
	A parte superior ficará organizado da seguinte forma, lado esquerdo
	Logo da Concessionária, lado direito logo da Montadora.

	A Parte inferior, será o menu de navegação com as principais abas
	conforme o exemplo abaixo.

![Imagem](https://res.cloudinary.com/midia9-group/image/upload/v1544474973/Screenshot_15_vjr6tb.png)

#### Menus
	Os menus serão a sintese das principais funcionalidades do Aplicativo. Eles ficarão organizados da seguinte forma

	1 - Página Inicial
	2 - Carros Novos
	3 - Carros Seminovos
	4 - Serviços da Concessionária
	5 - Informações da Concessionária

	Todas as funcionalidades serão explicadas a seguir

#### 1 Página Inicial
	A página inicial será secionada em 3 Partes. Seguindo o padrão adotado
	por outros aplicativos da mesma área de também com o intuito de manter a 
	navegação do usuário facilitada.
##### 1.1 Sessão 1
	A sessão 1 conterá imagens em formato de Banners rotativos, relacionados a ofertas,
	novidades e etc. Os banners serão alimentados via Painel atráves do CRM
	no módulo de Gerencimanento do Aplicativo (Em Desenvolvimento).
##### 1.2 Sessão 2
	Servirá como uma aba de Noticias da Concessionária, essas informações também serão cadastradas via Painel
	através do módulo de Blog. (API em Desenvolvimento).

##### 1.3 Sessão 3
	Será a sessão voltada para noticias da Montadora, alimentada via RSS que ainda está pendende o envio.

![Exemplo](https://res.cloudinary.com/midia9-group/image/upload/v1544474838/Screenshot_14_cfk6a4.png)





	







